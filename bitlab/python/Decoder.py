import struct


class Decoder:
    def decode_header(self, message, to_print=True):
        message_type = str(message[4:16], "UTF-8")

        if to_print is True:
            print(f'Magic: {str(message[:4].hex())}\n'
                  f'Message: {message_type}\n'
                  f'Payload length: {str(struct.unpack("I", message[16:20])[0])}\n'
                  f'Checksum: {str(message[20:24].hex())}\n')
        return message_type.rstrip('\00')

    def decode_version(self, message):
        version = struct.unpack("i", message[:4])[0]
        print(f'Version: {str(version)}\n'
              f'Services: {str(struct.unpack("Q", message[4:12])[0])}\n'
              f'Timestamp: {str(struct.unpack("q", message[12:20])[0])}\n'
              f'Services recv: {str(struct.unpack("Q", message[20:28])[0])}\n'
              f'IP Address recv: {str(struct.unpack(">16s", message[28:44])[0])}\n'
              f'Port recv: {str(struct.unpack(">H", message[44:46])[0])}')

        if version >= 106:
            print(f'Services from: {str(struct.unpack("Q", message[46:54])[0])}\n'
                  f'IP Address from: {str(struct.unpack(">16s", message[54:70])[0])}\n'
                  f'Port from: {str(struct.unpack(">H", message[70:72])[0])}\n'
                  f'Nonce: {str(struct.unpack("Q", message[72:80])[0])}\n'
                  f'User agent (length): {str(struct.unpack("B", message[80:81])[0])}\n'
                  f'User agent: {str(message[81:97], "UTF-8-SIG")}\n'
                  f'Starting height: {str(struct.unpack("i", message[97:101])[0])}\n')

        if version >= 70001:
            print(f'Relay: {str(struct.unpack("?", message[101:102])[0])}\n')
        return message[102:]

    def decode_ping(self, message):
        return struct.unpack("Q", message[24:32])[0]
