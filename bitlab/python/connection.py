import socket
import struct
import random
import sys

# Custom modules
import Messages
import Decoder


mess = Messages.Messages()
decoder = Decoder.Decoder()
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

HOST = "190.210.234.38"
PORT = 8333

s.connect((HOST, PORT))

received = []
sent = []

autoconnect = True
option = None
while True:
    if autoconnect is False:
        print('Choose an option:\n'
              '[1] Connect to a node (version and verack messages)\n'
              '[2] Ping message\n'
              '[3] Getaddr message\n'
              '[4] Decode previously sent messages\n'
              '[5] Decode previously received messages\n'
              '[6] Show IP and port of a node\n'
              '[7] Change IP and port of a node\n'
              '[8] Get genesis block\n'
              '[9] Alert message\n'
              '[10] Pong message\n'
              '[0] Exit\n')
        option = input('Choice: ')

    if option == '0':
        print('Script halt.')
        sys.exit()

    elif option == '1' or autoconnect:
        if autoconnect:
            print('** Autoconnecting to node! **')
            autoconnect = False
        ver = mess.version()
        s.send(ver)
        sent.append(ver)
        print("--Version message sent--")

        b = s.recv(2048)
        received.append(b)

        message = ""
        if len(b) >= 24:
            message = decoder.decode_header(b)
        b2 = s.recv(2048)

        if message == "version":
            print("--Version message received--")
            b2 = decoder.decode_version(b2)
            if len(b2) >= 24:
                message = decoder.decode_header(b2)

        b3 = s.recv(2048)
        if len(b3) != 0:
            message = decoder.decode_header(b3)

        if message == "verack":
            received.append(b3)
            print("--Verack message received--")
            verack = mess.verack()
            s.send(verack)
            sent.append(verack)
            print("--Verack message sent--")
            for i in range(5):
                b = s.recv(1024)
                if len(b) != 0:
                    received.append(b)
                    if b.find(b"\ping") != -1:
                        print('--Ping message received--')
                        nonce = b[24:]
                        s.send(mess.pong(nonce[:8]))
                        print('--Ping message sent--')
        input('Done! Press enter.')

    elif option == '2':
        s.recv(1024)
        s.recv(1024)
        ping = mess.ping()
        s.send(ping)
        sent.append(ping)
        b = s.recv(1024)
        print('--Ping message sent--')
        if len(b) != 0:
            print(b)
            message = decoder.decode_header(b)
            if message == "pong":
                print('--Pong message received--')
                received.append(b)
        input('Done! Press enter.')

    elif option == '3':
        b = s.recv(1024)
        print(b)
        getaddr = mess.getaddr()
        s.send(getaddr)
        sent.append(getaddr)
        b = s.recv(1024)
        print('--Getaddr message sent--')
        if len(b) != 0:
            message = decoder.decode_header(b)
            if message == "addr":
                print('--Addr message received--')
                received.append(b)
        input('Done! Press enter.')

    elif option == '4':
        for message in sent:
            print(message)
            print(decoder.decode_header(message, False))
        input('Done! Press enter.')

    elif option == '5':
        for message in received:
            print(message)
            print(decoder.decode_header(message, False))
        input('Done! Press enter.')

    elif option == '6':
        print(HOST + ":" + str(PORT))
        input('Done! Press enter.')

    elif option == '7':
        HOST = input()
        PORT = int(input())
        s.connect((HOST, PORT))
        input('Done! Press enter.')

    elif option == '8':
        genesis = mess.getgenesisblock()
        s.send(genesis)
        print('--Getblocks message sent--')

        for i in range(4):
            b = s.recv(1024)
            print(b)
        sent.append(genesis)
        input('Done! Press enter.')

    elif option == '9':
        alert = mess.alert()
        s.send(alert)
        print('--Alert message sent--')
        sent.append(alert)
        input('Done! Press enter.')

    elif option == '10':
        nonce = struct.pack("Q", random.getrandbits(64))
        pong = mess.pong(nonce)
        s.send(pong)
        print('--Pong message sent--')
        sent.append(pong)
        input('Done! Press enter.')

    else:
        print(f'There\'s no option like {option}.')
        input('Press enter.')
