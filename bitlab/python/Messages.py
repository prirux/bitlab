import struct
import random
import hashlib
import socket
import time


class Messages:
    def version(self):
        version = struct.pack("i", 70015)
        services = struct.pack("Q", 1)
        timestamp = struct.pack("q", int(time.time()))

        byte_list = []
        for i in range(10):
            byte_list.append(0)
        for i in range(2):
            byte_list.append(255)
        b = bytes(byte_list) + socket.inet_aton("127.0.0.1")

        addr_recv_services = struct.pack("Q", 0)  # services
        addr_recv_ip = b
        addr_recv_port = struct.pack(">H", 8333)

        addr_trans_services = struct.pack("Q", 0)  # services
        addr_trans_ip = b
        addr_trans_port = struct.pack(">H", 8333)

        nonce = struct.pack("Q", random.getrandbits(64))
        user_agent_bytes = struct.pack("B", 0)
        starting_height = struct.pack("i", 559222)
        relay = struct.pack("?", False)

        payload = (version + services + timestamp + addr_recv_services + addr_recv_ip
                   + addr_recv_port + addr_trans_services + addr_trans_ip +
                   addr_trans_port + nonce + user_agent_bytes + starting_height + relay)

        magic = bytes.fromhex("F9BEB4D9")
        command = ("version" + 5 * "\00").encode()
        length = struct.pack("I", len(payload))

        check = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[:4]

        msg = magic + command + length + check + payload
        return msg

    def verack(self):
        # header
        magic = bytes.fromhex("F9BEB4D9")
        command = ("verack" + 6 * "\00").encode()
        length = struct.pack("I", 0)
        checksum = bytes.fromhex("5DF6E0E2")
        msg = magic + command + length + checksum

        return msg

    def ping(self):
        nonce = struct.pack("Q", random.getrandbits(64))
        magic = bytes.fromhex("F9BEB4D9")
        command = ("ping" + 8 * "\00").encode()
        length = struct.pack("I", len(nonce))
        checksum = hashlib.sha256(hashlib.sha256(nonce).digest()).digest()[:4]
        msg = magic + command + length + checksum + nonce
        return msg

    def pong(self, nonce):
        magic = bytes.fromhex("F9BEB4D9")
        command = ("pong" + 8 * "\00").encode()
        length = struct.pack("I", len(nonce))
        checksum = hashlib.sha256(hashlib.sha256(nonce).digest()).digest()[:4]
        msg = magic + command + length + checksum + nonce
        return msg

    def getaddr(self):
        magic = bytes.fromhex("F9BEB4D9")
        command = ("getaddr" + 5 * "\00").encode()
        length = struct.pack("I", 0)
        checksum = hashlib.sha256(hashlib.sha256(bytes()).digest()).digest()[:4]
        msg = magic + command + length + checksum
        return msg

    def alert(self):
        version = struct.pack("i", 1)
        relay_until = struct.pack("q", int(time.time()))
        expiration = struct.pack("q", int(time.time())+1000)
        ID_int = 12832
        ID = struct.pack("i", ID_int)
        cancel = struct.pack("i", ID_int - 1)

        min_ver = struct.pack("i", 10000)
        max_ver = struct.pack("i", 12000)
        priority = struct.pack("i", 100)

        text = "Test alert".encode()

        payload = (version + relay_until + expiration + ID + cancel + min_ver
                   + max_ver + priority + text)
        magic = bytes.fromhex("F9BEB4D9")
        command = ("alert" + 7 * "\00").encode()
        length = struct.pack("I", len(payload))
        checksum = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[:4]
        message = magic + command + length + checksum + payload
        return message

    def addr(self, addr="153.125.129.187"):
        count = struct.pack("B", 1)
        print(count)
        timestamp = struct.pack("I", int(time.time()))
        print(timestamp)
        services = struct.pack("Q", 1)
        print(services)
        byte_list = []
        for i in range(10):
            byte_list.append(0)
        for i in range(2):
            byte_list.append(255)
        b = bytes(byte_list) + socket.inet_aton(addr)
        IP = b
        port = struct.pack(">H", 8333)
        payload = count + timestamp + services + IP + port

        magic = bytes.fromhex("F9BEB4D9")
        command = ("addr" + 8 * "\00").encode()
        length = struct.pack("I", len(payload))
        checksum = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[:4]
        msg = magic + command + length + checksum + payload
        return msg

    def getgenesisblock(self):
        magic = bytes.fromhex("F9BEB4D9")
        command = ("getblocks" + 3 * "\00").encode()
        version = struct.pack("i", 70015)
        hashcount = bytes.fromhex("01")
        blockheaderhashes = (32 * "\00").encode()
        stophash = (32 * "\00").encode()
        payload = version + hashcount + blockheaderhashes + stophash
        length = struct.pack("I", len(payload))
        checksum = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[:4]
        msg = magic + command + length + checksum + payload
        return msg
