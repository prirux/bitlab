package testapp.testsocket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class Server {

    private SocketChannel socketChannel = null;
    private int port = 16503;

    public void receiveObject() throws IOException{
        createSocketChannel();
        ObjectInputStream objectInputStream = new ObjectInputStream(socketChannel.socket().getInputStream());
        TestClass obj = null;
        try {
             obj = (TestClass) objectInputStream.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Otrzymano obiekt: " + obj.getMessage() + " " + obj.getValue());
        socketChannel.close();
    }

    private void createSocketChannel() throws IOException {
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.socket().bind(new InetSocketAddress(port));
        socketChannel = serverSocketChannel.accept();

        System.out.println("Połączono z " + socketChannel.getLocalAddress());
    }

    public static void main(String[] args) {
        Server server = new Server();
        try {
            server.receiveObject();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
