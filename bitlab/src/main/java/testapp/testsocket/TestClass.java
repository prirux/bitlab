package testapp.testsocket;

import java.io.Serializable;

public class TestClass implements Serializable {

    private int value;
    private String message;

    public TestClass(int value, String message){
        this.value = value;
        this.message = message;
    }

    public int getValue() {return value;}
    public String getMessage() {return message;}

}
