package testapp.testsocket;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.channels.SocketChannel;

public class Client {

    private int port = 16503;
    private String hostname = "localhost";
    private boolean isConnected = false;
    private SocketChannel socketChannel;
    private ObjectOutputStream outputStream;

    public static void main(String[] args) {
        Client client = new Client();
        try {
            client.sendObject();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public SocketChannel createChannel() throws IOException {
        SocketChannel socketChannel = SocketChannel.open();
        SocketAddress address = new InetSocketAddress(hostname, port);
        socketChannel.connect(address);
        return socketChannel;
    }

    void sendObject() throws IOException{
        while(!isConnected){
            socketChannel = createChannel();
            isConnected = true;
            outputStream = new ObjectOutputStream(socketChannel.socket().getOutputStream());
            TestClass testClass = new TestClass(156, "Halo");
            outputStream.writeObject(testClass);
        }
    }
}
